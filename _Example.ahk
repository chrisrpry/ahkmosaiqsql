﻿;-------------------------------------
;Innitiation
;-------------------------------------
	#NoENV 
	#SingleInstance Force
	#include MOSAIQSQL.ahk
;-------------------------------------
;Variables
;-------------------------------------
	WorkingLoc=%A_AppData%\%ToolName%
;-------------------------------------
;Working Directory
;-------------------------------------
	If (InStr(FileExist(WorkingLoc),"D")) 
		{}
		Else 
		{
			fileCreateDir, %A_AppData%\%toolName%
		}
	SetWorkingDir,%WorkingLoc%
;-------------------------------------
;Execute SQL Authentication program + DEBUG (ON/OFF)
;-------------------------------------
MOSAIQSQLAuth(1) ;<--1=Debug ON, 0=Debug OFF
WinWaitClose, SQL Authentication ;<Prevents this script from continuing past this point until the SQL Auth is complete.
;-------------------------------------
;SQL Query examples
;-------------------------------------
Query:="SELECT SUBSTRING(@@Version,22,4)"
SQLVersion:= performQuery(ConnectString, Query)
Query:="SELECT SUBSTRING(CAST(value AS varchar(38)),5,4) AS MQ_Version FROM sys.extended_properties where name = 'DB_Version'"
MQVersion:= performQuery(ConnectString, Query)